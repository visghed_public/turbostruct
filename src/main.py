import os

import yaml

from application.models.ble_model import BleModel
from application.models.data_model import DataModel
from application.cpp.data_classes import create as cpp_dataclass_create
from application.typesript.data_classes import process_models
from application.typesript.create_functions import create as typescript_create_func

from application.pythonize.data_classes import create as python_dataclass_create
from application.pythonize.create_functions import create as python_create_func

from application.cpp.create_functions import create as cpp_create_func
from application.validator.data_class import DataCacheValidator


complete_files = []
for root, dir_names, file_names in os.walk('./tests/config_files'):
    for f in file_names:
        file_name = os.path.join(root, f)
        if file_name.endswith('.yaml'):
            complete_files.append(file_name)


cache = DataCacheValidator()
for config_file in complete_files:

    with open(config_file, 'r') as file:
        docs = yaml.safe_load_all(file)

        for doc in docs:
            if doc is not None and 'kind' in doc:
                if doc['kind'] == 'DataModel':
                    turbos_struct_model = DataModel(**doc)
                    cache.add({config_file: turbos_struct_model})
                elif doc['kind'] == 'BleServices':
                    model = BleModel(**doc)

process_models(cache)
for item in list(cache.container.keys()):
    for model_name in list(cache.container[item].container.keys()):
        python_dataclass_create(cache.container[item].container[model_name], 'tests')
        cpp_dataclass_create(cache.container[item].container[model_name])
        # typescript_dataclass_create(cache.container[item].container[model_name])

cache.show_dependencies()

python_create_func(model)
cpp_create_func(model)
typescript_create_func(model)