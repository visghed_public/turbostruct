import re


# https://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-snake-case
def to_snake_case(name):
    name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    name = re.sub('__([A-Z])', r'_\1', name)
    name = re.sub('([a-z0-9])([A-Z])', r'\1_\2', name)
    return name.lower()


# https://stackoverflow.com/questions/19053707/converting-snake-case-to-lower-camel-case-lowercamelcase
def to_camel_case(snake_str):
    x = ''
    for part in snake_str.split("_"):
        x += part[0].upper() + part[1:]
    return x


def to_lower_camel_case(snake_str):
    # We capitalize the first letter of each component except the first one
    # with the 'capitalize' method and join them together.
    camel_string = to_camel_case(snake_str)
    return snake_str[0].lower() + camel_string[1:]


# https://www.techieclues.com/blogs/converting-a-string-to-kebab-case-in-python
def to_kebab_case(input_string):
    kebab_case = re.sub(r'(?<!^)(?=[A-Z])', '-', input_string).lower()
    return kebab_case
