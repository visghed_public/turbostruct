def struct_2_dict(struct):
    result = {}

    def get_value(value):
        if (type(value) not in [int, float, bool]) and not bool(value):
            # it's a null pointer
            value = None
        elif hasattr(value, "_length_") and hasattr(value, "_type_"):
            # Probably an array
            # print value
            value = get_array(value)
        elif hasattr(value, "_fields_"):
            # Probably another struct
            value = struct_2_dict(value)
        return value

    def get_array(array):
        ar = []
        for item in array:
            item_value = get_value(item)
            ar.append(item_value)
        return ar

    for f in struct._fields_:
        field = f[0]
        attributes = getattr(struct, field)
        # if the type is not a primitive and it evaluates to False ...
        attributes = get_value(attributes)
        result[field] = attributes
    return result
