import os
from datetime import datetime

from application.utils.cases import to_snake_case


def create(turbos_struct_model):
    properties = turbos_struct_model.properties
    upper_class_name = properties.name.upper()
    filename = f'./tests/cpp/data_classes/{to_snake_case(properties.name)}.h'
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, mode='w', encoding='utf-8') as f:
        f.write(f'#ifndef _{upper_class_name}_' + '\n')
        f.write(f'#define _{upper_class_name}_' + '\n')
        f.write('' + '\n')
        for attribute in turbos_struct_model.properties.attributes:
            if attribute.reference_type is not None:
                f.write(
                    f'#include "{to_snake_case(attribute.reference_type)}.h"' + '\n')

        f.write('' + '\n')
        f.write(f'// Autogenerated by Turbostruct on {datetime.utcnow()}' + '\n')
        f.write('' + '\n')
        f.write('// Description:' + '\n')
        f.write(f'// {properties.description}' + '\n')
        f.write('' + '\n')
        f.write('' + '\n')
        f.write(f'struct {properties.name}' + ' {' + '\n')
        for attribute in properties.attributes:
            multiplier = ""
            if attribute.multiplier is not None:
                multiplier = f'[{attribute.multiplier}]'
            default = ""
            if attribute.default is not None:
                if str(attribute.default) == 'False':
                    default = ' = false'
                elif str(attribute.default) == 'True':
                    default = ' = true'
                elif attribute.default.__class__ == str:
                    if ',' in attribute.default and attribute.multiplier is not None:
                        default = ' = ' + '{ ' + attribute.default + ' }'
                    else:
                        default = f' = "{attribute.default}"'
                else:
                    default = f' = {attribute.default}'

            if attribute.type is not None:
                attribute_type = attribute.type.cpp
            else:
                attribute_type = attribute.reference_type

            f.write(f'    {attribute_type} {attribute.name}{multiplier}{default};' +
                    f'  // {attribute.description}' + '\n')
        f.write('};' + '\n')
        f.write('#endif')
