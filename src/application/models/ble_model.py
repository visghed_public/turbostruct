from enum import Enum

from pydantic import BaseModel, Field, UUID4, model_validator, field_validator

from application.models.data_model import TypeConversion


class Kind(Enum):
    BleServices = 'BleServices'


class BleProperties(Enum):
    def __new__(cls, *args, **kwargs):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    def __init__(self, nimble, python):
        self.nimble = nimble
        self.python = python

    read = 'NIMBLE_PROPERTY::READ', 'read_gatt_char'
    notify = 'NIMBLE_PROPERTY::WRITE', 'write_gatt_char'
    write = 'NIMBLE_PROPERTY::NOTIFY',  'start_notify'


class DataExchange(BaseModel):
    type: TypeConversion | None = None
    reference_type: str | None = None
    multiplier: int | None = None
    default: str | int | None = None

    @model_validator(mode='after')
    def check_typ_vs_reference(self):
        default_type = self.type
        reference_type = self.reference_type
        if default_type is None and reference_type is None:
            raise ValueError('either type or reference_type must be set')
        if default_type is not None and reference_type is not None:
            raise ValueError('either type or reference_type must be set')
        return self

    @field_validator(__field='type', mode='before')
    def type_validation(cls, v):
        if v is None:
            return None
        if v not in [type_conv.name for type_conv in TypeConversion]:
            raise ValueError(f'input: {v}, supported values are: {[type_conv.name for type_conv in TypeConversion]}')
        return TypeConversion[v]


class Response(BaseModel):
    identifier: str
    send: DataExchange | None = None
    receive: DataExchange | None = None


class Characteristic(BaseModel):
    name: str
    uuid: UUID4
    properties: list[BleProperties]
    responses: list[Response]

    @field_validator(__field='properties', mode='before')
    def type_validation(cls, v: list[str]):
        if v is None or len(v) == 0:
            raise ValueError('properties cannot be empty')
        properties: list[BleProperties] = []
        for prop in v:
            if prop not in [ble_prop.name for ble_prop in BleProperties]:
                raise ValueError(f'input: {v}, supported values are: {[ble_prop.name for ble_prop in BleProperties]}')
            else:
                properties.append(BleProperties[prop])
        return properties


class Service(BaseModel):
    name: str
    uuid: UUID4
    characteristics: list[Characteristic]


class Pooling(BaseModel):
    serviceName: str
    start: list[str] | None = None
    end: list[str] | None = None


class BleModel(BaseModel):
    kind: Kind = Field()
    name: str
    services: list[Service]
    pooling: list[Pooling]
