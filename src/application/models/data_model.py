from enum import Enum

from pydantic import BaseModel, Field, field_validator, model_validator


class Kind(Enum):
    DataModel = 'DataModel'


class TypeConversion(Enum):
    def __new__(cls, *args, **kwargs):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    def __init__(self, cpp, c_type, python, javascript, format_):
        self.cpp = cpp
        self.c_type = c_type
        self.python = python
        self.javascript = javascript
        self.format = format_

    boolean = 'bool', 'c_bool', 'bool', 'Boolean',  '?'
    float = 'float', 'c_float', 'float', 'Number',  'f'
    uint16_t = 'uint16_t', 'c_uint16', 'int', 'Number',  'I'
    uint8_t = 'uint8_t', 'c_uint8', 'int', 'Number', 'H'
    int64_t = 'int64_t', 'c_int64', 'int', 'Number', 'i'
    char = 'char', 'c_char', 'str', 'String', 's'
    byte = 'byte', 'c_byte', 'bytes', 'BytesArray', 'x'


class Attribute(BaseModel):
    name: str
    unit: str | None = None
    description: str | None = None
    type: TypeConversion | None = None
    multiplier: int | None = None
    reference_type: str | None = None
    external_dependencies: bool = False
    default: str | int | bool = None

    @model_validator(mode='after')
    def check_typ_vs_reference(self):
        default_type = self.type
        reference_type = self.reference_type
        if default_type is None and reference_type is None:
            raise ValueError('either type or reference_type must be set')
        if default_type is not None and reference_type is not None:
            raise ValueError('either type or reference_type must be set')
        return self

    @field_validator(__field='type', mode='before')
    def type_validation(cls, v):
        if v is None:
            return None
        if v not in [type_conv.name for type_conv in TypeConversion]:
            raise ValueError(f'input: {v}, supported values are: {[type_conv.name for type_conv in TypeConversion]}')
        return TypeConversion[v]


class Properties(BaseModel):
    name: str
    description: str
    attributes: list[Attribute]


class DataModel(BaseModel):
    kind: Kind = Field()
    properties: Properties | None = None
