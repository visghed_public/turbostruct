from application.models.data_model import DataModel
from copy import deepcopy


class Class2Model:
    def __init__(self, item: DataModel):
        self.container: dict[str, DataModel] = {item.properties.name: item}
        self.all_class_names: [str] = []

    def add(self, item: DataModel):
        if item.properties.name in self.get_class_list():
            raise ValueError(f'class already defined {item.properties.name}')
        else:
            self.container[item.properties.name] = item

    def get_class_list(self):
        self.all_class_names = [item for item in self.container.keys()]
        return self.all_class_names

    def get_external_dependencies(self) -> [str]:
        external_dependencies: list[str] = []
        for model in self.container.values():
            for attribute in model.properties.attributes:
                if attribute.reference_type is not None:
                    external_dependencies.append(attribute.reference_type)
        return external_dependencies


class DataCacheValidator:
    def __init__(self):
        self.container: dict[str, Class2Model] = {}
        self.all_class_names: [str] = []

    def add(self, item: dict[str, DataModel]) -> None:
        the_key = list(item.keys())[0]
        if self.has_class(the_key):
            raise ValueError(f'class {list(item.keys())[0]} already defined')
        else:
            if the_key not in list(self.container.keys()):
                self.container[the_key] = Class2Model(item[the_key])
            else:
                self.container[the_key].add(item[the_key])

    def has_class(self, class_name: str):
        self.all_class_names = []
        for clazz in list(self.container.keys()):
            self.all_class_names += self.container.get(clazz).get_class_list()

        return class_name in self.all_class_names

    def show_dependencies(self):
        chain = self.calculate_dependencies()
        print('Dependencies:')
        self.print_recursive_dependencies(chain, 0)

    @classmethod
    def print_recursive_dependencies(cls, chain, level):
        level += 1
        for item in chain:
            if item['show']:
                print(f'{"    " * level}⮡ {item["name"]}')
                cls.print_recursive_dependencies(item['dependencies'], level)

    @classmethod
    def get_recursive_class(cls, dependencies, class_name):
        for dependency in dependencies:
            if dependency['name'] == class_name:
                dependency['show'] = False
                copy_dep = deepcopy(dependency)
                copy_dep['show'] = True
                return copy_dep
            cls.get_recursive_class(dependency['dependencies'], class_name)

    @classmethod
    def rearrange_list(cls, keeper) -> list | None:
        for index, item in enumerate(keeper):
            largest_index = index
            for dep in item['dependencies']:
                for index2, item2 in enumerate(keeper):
                    if item2['name'] == dep:
                        if index2 > largest_index:
                            largest_index = index2

            if index < largest_index:
                del keeper[index]
                keeper.insert(largest_index, item)
                return keeper
        return None

    def calculate_dependencies(self) -> list:
        dependency_chain = []
        class_list = {}
        keeper: list = []
        # fill keeper with data
        for clazz in list(self.container.keys()):
            for item in list(self.container.get(clazz).container.values()):
                external_dependencies = []
                for attribute in item.properties.attributes:
                    if attribute.reference_type is not None:
                        external_dependencies.append(attribute.reference_type)
                class_list[item.properties.name] = item
                keeper.append({'name': item.properties.name,
                               'dependencies': external_dependencies,
                               'show': True, 'used': False})

        runner = True
        while runner:
            new_list = self.rearrange_list(keeper)
            if new_list is not None:
                keeper = new_list
            else:
                runner = False

        for clazz in keeper:
            item = class_list[clazz["name"]]

            tree_obj = {'name': item.properties.name, 'dependencies': [], 'show': True}
            for attribute in item.properties.attributes:
                if attribute.reference_type is not None:
                    if attribute.reference_type not in self.all_class_names:
                        raise ValueError(f'class {attribute.reference_type} not defined')
                    dep = self.get_recursive_class(dependency_chain, attribute.reference_type)

                    tree_obj['dependencies'].append(dep)

            dependency_chain.append(tree_obj)

        return dependency_chain
