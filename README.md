# Turbo Struct

Convert C and C++ structs to python format. Reads C/C++ header files and extracts all structs and converts them to Python
c type structs (uses ctypes.Structure) package and makes a conversion to Pydantic format.


```c++
# imu_config.h
struct ImuConfig {
  uint16_t pulling_rate_quat_ms;
  uint8_t pulling_rate_acc_ms = 10;
  uint16_t pulling_rate_class_ms = 10;
};
```

will be converted to:

```python
class ImuConfig(CTypeWrapper):
    structure = {
         "pulling_rate_quat_ms": TypeDeclaration(ctypes.c_uint16),
         "pulling_rate_acc_ms": TypeDeclaration(ctypes.c_uint8, default=10),
         "pulling_rate_class_ms": TypeDeclaration(ctypes.c_uint16, default=10),
        }

    _fields_ = get_fields(structure)
```